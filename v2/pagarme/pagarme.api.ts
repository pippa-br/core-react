import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const addPaymentPagarme = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_PAYMENT_PAGARME_SERVER, setting);
	
	return result;
}

const addCardPagarme = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_CARD_PAGARME_SERVER, setting);
	
	return result;
}

export { 
	addPaymentPagarme,
	addCardPagarme 
}