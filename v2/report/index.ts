import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const weighingReport = async (setting: ISetting) => {
    const result = await call(Types.WEIGHING_REPORT_SERVER, setting);
    return result;
};

export { weighingReport };