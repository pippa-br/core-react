import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";

const useCollectionFavorite = async (setting:ISetting, onSuccess?:any) => 
{	
	useEffect(() => 
	{
		call(Types.COLLECTION_FAVORITE_SERVER, setting).then((result) => 
		{
			onSuccess(result.collection || [])
		});	
	}, []);
}

const useGetFavorite = async (setting:ISetting, onSuccess?:any) => 
{
	useEffect(() => 
	{
		call(Types.GET_FAVORITE_SERVER, setting).then((result) => 
		{
			onSuccess(result.status)
		});	
	}, []);
}

const setFavorite = async (setting:ISetting) => 
{
	const result = await call(Types.SET_FAVORITE_SERVER, setting);
	
	return result;
}

export { useCollectionFavorite, useGetFavorite, setFavorite }