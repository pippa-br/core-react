export default interface IUser {
  cpf: string;
  phone: number;
  address: any;
  name: string;
  referencePath: string;
}

export interface UserProps {
  name: string;
  store: {
    name: string;
    code: string;
    id: string;
    referencePath: string;
    appid: string;
    accid: string;
  };
  type: string;
  cpf: string;
  cnpj: string;
  referencePath: string;
}
