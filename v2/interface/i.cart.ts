export default interface ICart 
{
    creditClient: number;
    discountClient: number;
    referencePath : string;
    items: any;
    paymentMethod: any;
    coupon: any;
    shipping: any;
    totalItems: number;
    totalDiscount: number;
    totalInterest:number;
    total: number;
    store: {
        accid: string;
        appid: string;
        code: string;
        id: string;
        logo: any;
        name: string;
        referencePath: string;
        phone: string;
    };
    client: {
        accid: string;
        appid: string;
        id: string;
        name: string;
        referencePath: string;
    };
}
