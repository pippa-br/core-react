import { CategoryProps } from "./i.category";
import { StoreProps } from "./i.store";

export interface ProductCategoryProps {
  name: string;
  id: string;
  referencePath: string;
}

export interface ProductSpecificationProps {
  value: string;
  label: string;
}

export interface ProductVariantProps {
  items: [
    {
      value: string;
      label: string;
      id: string;
    }
  ];
}

export interface ProductImageProps {
  [color: string]: {
    images: {
      colid: string;
      id: string;
      _url: string;
      name: string;
      type: string;
    }[];
  };
}

export interface ProductImagesProps {
  data: ProductImageProps;
}

export interface ProductProps {
  id: string;
  name: string;
  code: string;
  categories: CategoryProps[];
  description: string;
  variant: ProductVariantProps[];
  images: ProductImagesProps;
  price: number;
  promotionalPrice: number;
  specification: ProductSpecificationProps[];
  store: StoreProps;
  referencePath: string;
}
