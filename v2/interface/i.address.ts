export interface AddressProps {
  zipcode: string;
  complement: string;
  country: {
    value: "br";
    label: "Brasil";
    id: "br";
  };
  state: string;
  district: string;
  street: string;
  city: string;
  housenumber: string;
}

export interface AddressesProps {
  id: string;
  name: string;
  referencePath: string;
  address: AddressProps;
}
