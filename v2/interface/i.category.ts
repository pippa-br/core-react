export interface CategoryProps {
    id: string;
    name: string;
    _parent: {
        id: string;
        name: string;
        image: string;
        mobile: string;
        referencePath: string;
        appid: string;
        accid: string;
    },
    _children: string;
    icon: string;
    referencePath: string;
}



  