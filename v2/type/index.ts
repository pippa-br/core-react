import { BASE_URL_API } from "../../../setting/setting";

export interface IType {
  	label: string;
  	value: string;
}

export default class Types 
{
  	// CONTAINER
  	static FLAT_CONTAINER    = { id: "flat", label: "Flat", value: "flat" };
  	static SEGMENT_CONTAINER = {
    	id    : "segment",
    	label : "Segment",
    	value : "segment",
  	};

	//MATRIX
	static COMBINE_MATRIX  = { value : 'combine',  label : 'Combinação', id : 0 };
	static SEPARATE_MATRIX = { value : 'separate', label : 'Separação',  id : 1 };
	static VARIANT_DEFAULT = [{ items : [{id : '_default',value : '_default'}]}];

  	// FIELD
  	static TEXT_FIELD  = { id: "text",  label: "Texto",  value: "text" };
  	static EMAIL_FIELD = { id: "email", label: "E-mail", value: "email" };

	// PAGARME
	static ADD_PAYMENT_PAGARME_SERVER = `${BASE_URL_API}/pagarmeApi/addPayment`;
	static ADD_CARD_PAGARME_SERVER    = `${BASE_URL_API}/pagarmeApi/addCard`;

	// UTIL
	static PROXY_UTIL_SERVER = `${BASE_URL_API}/utilApi/proxy`;
	static RSS_UTIL_SERVER = `${BASE_URL_API}/utilApi/rssParser`;

	// FAVORITE
	static COLLECTION_FAVORITE_SERVER = `${BASE_URL_API}/favoriteApi/collection`;
	static GET_FAVORITE_SERVER 		  = `${BASE_URL_API}/favoriteApi/get`;
	static SET_FAVORITE_SERVER 		  = `${BASE_URL_API}/favoriteApi/set`;

  	// AUTH	  
	static GET_TOKEN_LOGIN_AUTH_SERVER = `${BASE_URL_API}/authV2/getTokenLogin`;
  	static LOGIN_AUTH_SERVER 	  	   = `${BASE_URL_API}/authV2/login`;
  	static LOGOUT_AUTH_SERVER 	  	   = `${BASE_URL_API}/authV2/logout`;
	static CANCELED_AUTH_SERVER 	   = `${BASE_URL_API}/authV2/canceled`;
  	static GET_LOGGED_AUTH_SERVER 	   = `${BASE_URL_API}/authV2/getLogged`;
	static IS_LOGGED_AUTH_SERVER  	   = `${BASE_URL_API}/authV2/isLogged`;
	static VERIFY_LOGIN_AUTH_SERVER    = `${BASE_URL_API}/authV2/verifyLogin`;	

	// DOCUMENT
  	static COLLECTION_DOCUMENT_SERVER		 = `${BASE_URL_API}/documentV2/collection`;
  	static COLLECTION_OWNER_DOCUMENT_SERVER  = `${BASE_URL_API}/documentV2/collectionOwner`;
	static COUNT_DOCUMENT_SERVER			 = `${BASE_URL_API}/documentV2/count/`;
  	static GET_DOCUMENT_SERVER 		 		 = `${BASE_URL_API}/documentV2/get`;
  	static GET_DOCUMENT_FRONT 		 		 = `${BASE_URL_API}/documentV2/get`;
  	static COLLECTION_PRODUCT_SERVER 		 = `${BASE_URL_API}/ecomApi/collectionProduct`;
  	static GET_PRODUCT_SERVER 		 		 = `${BASE_URL_API}/ecomApi/getProduct`; 		
  	static ADD_DOCUMENT_SERVER 		 		 = `${BASE_URL_API}/documentV2/add`;
	static ADD_REORDER_SERVER 		 	     = `${BASE_URL_API}/ecomApi/addReorder`;
  	static SET_DOCUMENT_SERVER		 		 = `${BASE_URL_API}/documentV2/set`;
	static INCREMENT_DOCUMENT_SERVER		 = `${BASE_URL_API}/documentV2/increment`;
	static SET_VIEWS_SERVER		 		 	 = `${BASE_URL_API}/documentV2/setViews`;
  	static DELETE_DOCUMENT_SERVER 	 		 = `${BASE_URL_API}/documentV2/set`;
  	static UPLOAD_STORAGE_SERVER 	  		 = `${BASE_URL_API}/storageV2/upload2`;
	static UPLOAD_TEMP_SERVER 	  		 = `${BASE_URL_API}/storageV2/uploadTemp`;
	static UPLOAD_MAKE_PUBLIC_STORAGE_SERVER = `${BASE_URL_API}/storageV2/uploadMakePublic`;  
	static UPLOAD_BASE_64 = `${BASE_URL_API}/storageV2/base64Upload`;
	static ADD_USER_AUTH_SERVER 			 = `${BASE_URL_API}/authV2/addUser`;
	static ADD_USER_WITH_TOKEN_SERVER		 = `${BASE_URL_API}/authV2/addUserWithToken`;
	static SET_USER_AUHT_SERVER 			 = `${BASE_URL_API}/authV2/setUser`;
	static GET_USER_AUHT_SERVER 			 = `${BASE_URL_API}/authV2/getUser`;
	static GET_ACCOUNT_SERVER 				 = `${BASE_URL_API}/accountV2/get`;
	static RECOVERY_PASSWORD_SERVER 	 	 = `${BASE_URL_API}/authV2/recoveryPassword`;	
	static CALCULATE_SHIPPING_SERVER   		 = `${BASE_URL_API}/shippingApi/calculateShippings`;
	static GET_TRACK_CORREIOS_SERVER   		 = `${BASE_URL_API}/correiosApi/track`;

	//ORDER
	static COLLECTION_ORDER_SERVER 		 	 = `${BASE_URL_API}/orderApi/collection`;
  	static GET_ORDER_SERVER 		 		 = `${BASE_URL_API}/orderApi/get`;
	static SET_ITEM_ORDER_SERVER 		 	 = `${BASE_URL_API}/orderApi/setItem`;
	static APPROVED_ORDER_SERVER 		 	 = `${BASE_URL_API}/orderApi/approved`;
	static CANCELED_ORDER_SERVER 		 	 = `${BASE_URL_API}/orderApi/canceled`;
	static SET_STATUS_BY_STORE_ORDER_SERVER  = `${BASE_URL_API}/orderApi/setSatusByStore`;
	static SPLIT_BY_STORE_ORDER_SERVER 		 = `${BASE_URL_API}/orderApi/splitByStore`;

	// PRODUCT 2
	static COLLECTION_PRODUCT2_SERVER 		 = `${BASE_URL_API}/productApi/collection`;
  	static GET_PRODUCT2_SERVER 		 		 = `${BASE_URL_API}/productApi/get`; 	

	//CART
	static GET_CART_SERVER 			 		 	= `${BASE_URL_API}/cartV2/getCart/`; 
	static SET_ITEM_CART_SERVER 	 		 	= `${BASE_URL_API}/cartV2/setItemCart/`;
	static SET_ITEMS_CART_SERVER 	 		 	= `${BASE_URL_API}/cartV2/setItemsCart/`;
	static DEL_ITEM_CART_SERVER 	 		 	= `${BASE_URL_API}/cartV2/delItemCart/`;
	static DEL_GIFT_CART_SERVER 	 		 	= `${BASE_URL_API}/cartV2/delGiftCart/`;
	static DEL_ITEMS_BY_STORE_CART_SERVER 	 	= `${BASE_URL_API}/cartV2/delItemsByStoreCart/`;	
	static SET_SHIPPING_METHOD_CART_SERVER   	= `${BASE_URL_API}/cartV2/setShippingCart/`;
	static SET_PAYMENT_METHOD_CART_SERVER	 	= `${BASE_URL_API}/cartV2/setPaymentMethodCart/`;
	static SET_CREDITCARD_REFERENCE_CART_SERVER = `${BASE_URL_API}/cartV2/setCreditCardReferenceCart/`;
	static SET_CREDITCARD_CART_SERVER 			= `${BASE_URL_API}/cartV2/setCreditCardCart/`;
	static SET_ADDRESS_REFERENCE_CART_SERVER 	= `${BASE_URL_API}/cartV2/setAddressReferenceCart/`;
	static SET_ADDRESS_CART_SERVER 				= `${BASE_URL_API}/cartV2/setAddressCart/`;
	static SET_ATTACHMENT_CART_SERVER 		 	= `${BASE_URL_API}/cartV2/setAttachmentCart/`;
	static SET_INSTALLMENT_CART_SERVER 	 	 	= `${BASE_URL_API}/cartV2/setInstallmentCart/`;
	static SET_STORE_SERVER 	 	 			= `${BASE_URL_API}/cartV2/setStoreCart/`;
	static CLEAR_CART_SERVER 	 	 		 	= `${BASE_URL_API}/cartV2/clearCart/`;	
	static CHECKOUT_CART_SERVER 			 	= `${BASE_URL_API}/cartV2/checkoutCart/`;
	static SET_COUPON_CART 			 			= `${BASE_URL_API}/cartV2/setCouponCart/`;
	static GET_CART_ITEMS_QUANTITY_SERVER	 	= `${BASE_URL_API}/cartV2/countItemsCart/`; 
	static VALIDATE_CART_SERVER 	 	 		= `${BASE_URL_API}/cartV2/validateCart/`;
	static MERGE_CART_SERVER 	 	 			= `${BASE_URL_API}/cartV2/mergeCart/`;
	static MERGE_ALL_CART_SERVER 	 	 		= `${BASE_URL_API}/cartV2/mergeAllCart/`;
	static GET_GLOBAL_CONTEXT_SERVER	 		= `${BASE_URL_API}/cartV2/getGlobalContext/`;
	static CALCULATE_ZIP_CODE_SERVER 		 	= `${BASE_URL_API}/cartV2/calculateZipcodeCart/`;
	static DEL_COUPON_CART 			 			= `${BASE_URL_API}/cartV2/delCouponCart/`;
	static DEL_DISCOUNT_CLIENT_CART 			= `${BASE_URL_API}/cartV2/delDiscountClientCart/`;
	static SET_DISCOUNT_CLIENT_CART 			= `${BASE_URL_API}/cartV2/setDiscountClientCart/`;

	// MFM calls
	static WEIGHING_REPORT_SERVER				= `${BASE_URL_API}/mfmApi/weighingReport/`;
}
