import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const calculateShipping = async (setting:ISetting) => 
{
	const result = await call(Types.CALCULATE_SHIPPING_SERVER, setting);
	
	return result;
}

const getTrackCorreios = async (setting:ISetting) => 
{
	const result = await call(Types.GET_TRACK_CORREIOS_SERVER, setting);
	
	return result;
}

export { calculateShipping, getTrackCorreios }