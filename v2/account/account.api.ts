import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";

const useGetAccount = async (setting:ISetting, onSuccess:any) => 
{
	useEffect(() => 
	{
		call(Types.GET_ACCOUNT_SERVER, setting).then(result => 
		{
			onSuccess(result.data)
		});
	}, []);	
}

const getAccount = async (setting:ISetting, onSuccess?:any) => 
{
	const result = await call(Types.GET_ACCOUNT_SERVER, setting);
	
	return result;
}

export { getAccount, useGetAccount }
