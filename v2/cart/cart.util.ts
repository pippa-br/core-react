export const firstImageItemCart = (item:any) =>
{
	if(item)
	{
		for(const key in item.images)
		{
			return item.images[key]; // TEM QUE RENORMA OBJETO
		}	
	}
}

export const hasVariantItemCart = (item: any, position: number) => {
	if (item && item.variant && item.variant.length > position) {
	  return item.variant[position];
	}
};

export const variantItemCart = (item: any, position: number) => {
  if (item && item.variant && item.variant.length > position) {
    return item.variant[position].label;
  }
};
