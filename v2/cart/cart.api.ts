import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";

const useGetCart = (setting:ISetting, onSuccess:any) => 
{
	useEffect(() => 
	{
		call(Types.GET_CART_SERVER, setting).then((result) => 
		{
			onSuccess(result.data)
		});	
	}, []);	
}

const SetStoreCart = async (setting:ISetting, onSuccess?:any) => 
{	
	useEffect(() => 
	{
		call(Types.SET_STORE_SERVER, setting).then((result) => 
		{
			onSuccess(result.data)
		});	
	}, []);
}

const validateCart = async (setting: ISetting) => 
{
	const result = await call(Types.VALIDATE_CART_SERVER, setting);
	return result;
};

const mergeCart = async (setting: ISetting) => 
{
	const result = await call(Types.MERGE_CART_SERVER, setting);
	return result;
};

const mergeAllCart = async (setting: ISetting) => 
{
	const result = await call(Types.MERGE_ALL_CART_SERVER, setting);
	return result;
};

const setInstallmentsCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_INSTALLMENT_CART_SERVER, setting);
	
	return result;
}

const checkoutCart = async (setting:ISetting) => 
{
	const result = await call(Types.CHECKOUT_CART_SERVER, setting);
	
	return result;
}

const setCouponCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_COUPON_CART, setting);
	
	return result;
}

const setAddressCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_ADDRESS_CART_SERVER, setting);
	
	return result;
}

const setShippingMethodCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_SHIPPING_METHOD_CART_SERVER, setting);
	
	return result;
}

const setPaymentMethodCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_PAYMENT_METHOD_CART_SERVER, setting);
	
	return result;
}

const setCreditCardCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_CREDITCARD_CART_SERVER, setting);
	
	return result;
}

const setAttachmentCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_ATTACHMENT_CART_SERVER, setting);
	
	return result;
}

const clearCart = async (setting:ISetting) => 
{
	const result = await call(Types.CLEAR_CART_SERVER, setting);
	
	return result;
}

const setItemCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_ITEM_CART_SERVER, setting);
	
	return result;
}

const setItemsCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_ITEMS_CART_SERVER, setting);
	
	return result;
}

const delGiftCart = async (setting:ISetting) => 
{
	const result = await call(Types.DEL_GIFT_CART_SERVER, setting);
	
	return result;
}

const delItemCart = async (setting:ISetting) => 
{
	const result = await call(Types.DEL_ITEM_CART_SERVER, setting);
	
	return result;
}

const delCouponCart = async (setting:ISetting) => 
{
	const result = await call(Types.DEL_COUPON_CART, setting);
	
	return result;
}

const delDiscountClientCart = async (setting:ISetting) => 
{
	const result = await call(Types.DEL_DISCOUNT_CLIENT_CART, setting);
	
	return result;
}

const setDiscountClientCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_DISCOUNT_CLIENT_CART, setting);
	
	return result;
}

const delItemsByStoreCart = async (setting:ISetting) => 
{
	const result = await call(Types.DEL_ITEMS_BY_STORE_CART_SERVER, setting);
	
	return result;
}

const setAddressReferenceCart = async (setting:ISetting) => 
{
	const result = await call(Types.SET_ADDRESS_REFERENCE_CART_SERVER, setting);
	
	return result;
}

const setCreditCardReferenceCart = async (setting: ISetting) => {
	const result = await call(Types.SET_CREDITCARD_REFERENCE_CART_SERVER, setting);
  
	return result;
};

const calculateZipCodeCart = async (setting: ISetting) => {
	const result = await call(Types.CALCULATE_ZIP_CODE_SERVER, setting);
  
	return result;
};


export { 
	useGetCart,
	setAddressReferenceCart,
	SetStoreCart, 
	setCreditCardReferenceCart, 
	setItemsCart,
	delGiftCart, 
	delItemCart, 
	checkoutCart, 
	clearCart, 
	setAddressCart, 
	setInstallmentsCart, 
	setItemCart,
	setShippingMethodCart, 
	setAttachmentCart, 
	setPaymentMethodCart, 
	setCreditCardCart, 
	setCouponCart, 
	validateCart,
	calculateZipCodeCart,
	delItemsByStoreCart,
	delCouponCart,
	delDiscountClientCart,
	setDiscountClientCart,
	mergeCart,
	mergeAllCart
}