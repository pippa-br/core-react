import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";

const LoadVideosByChannelYoutube = (setting:any) => 
{
	let baseUrl = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=' + setting.channelId + '&key=' + setting.apiKey + '&q=' + (setting.term ? setting.term : '') + '&maxResults=' + (setting.perPage ? setting.perPage : 24) + '&order=date';

	if(setting.pageToken)
	{
		baseUrl += "&pageToken=" + setting.pageToken;
	}

	//baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

	const data = {
		url : baseUrl,
	};

	return call(Types.PROXY_UTIL_SERVER, data)
}

const LoadVideosByPlaylistYoutube = (setting:any) => 
{
	let baseUrl = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=' + setting.playlistId + '&key=' + setting.apiKey + '&q=' + (setting.term ? setting.term : '') + '&maxResults=' + (setting.perPage ? setting.perPage : 24) + '&order=date';

	if(setting.pageToken)
	{
		baseUrl += "&pageToken=" + setting.pageToken;
	}

	//baseUrl = 'https://us-central1-facilities-use.cloudfunctions.net/apiProxy?url=' + encodeURIComponent(baseUrl);

	const data = {
		url : baseUrl,
	};

	return call(Types.PROXY_UTIL_SERVER, data)
}

const GetVideosByChannelYoutube = (setting:any, onSuccess:any) => 
{
	useEffect(() => 
	{
		LoadVideosByChannelYoutube(setting).then(result => 
		{
			onSuccess(result);
		})
	}, []);	
}

const GetVideosByPlaylistYoutube = (setting:any, onSuccess:any) => 
{
	useEffect(() => 
	{
		LoadVideosByPlaylistYoutube(setting).then(result => 
		{
			onSuccess(result);
		})
	}, []);	
}

export { 
	GetVideosByChannelYoutube, 
	LoadVideosByChannelYoutube, 
	GetVideosByPlaylistYoutube 
}