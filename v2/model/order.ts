import BaseModel from "./base.model";

export default class Order extends BaseModel
{
	validateReorder()
	{
		let days = 0

		if(this.reorderExpiryDate)
		{
			// const dateStart = moment();
			// const dateEnd   = moment(this.reorderExpiryDate, 'DD/MM/YYYY');
	
			//days = dateEnd.diff(dateStart, 'days');
		}

		return this.statusPayment == 'Pago' && this.hasEnotas && !this.hasReorder && days >= 0;			
	}
}