import { useEffect } from "react";

class UseEvents {
  static events  : any = {};
  static dispatchs : any = {};
}

const ClearEvent = () => {

  useEffect(() => {
    // console.log("clearEvent");

    UseEvents.events = {};
    UseEvents.dispatchs = {};
  }, []);
};

const DispatchEvent = (key: any, data?: any) => 
{
  UseEvents.dispatchs[key] = data;

  // console.log("dispatchEvent", key, UseEvents.events, UseEvents.dispatchs);

  if (UseEvents.events[key]) {
    for (const callback of UseEvents.events[key]) {
      callback(data);
    }
  }
};

const OnEvent = (key: any, callback: any) => {

  useEffect(() => {
    // console.log("onEvent", key, UseEvents.events, UseEvents.dispatchs);

    if (!UseEvents.events[key]) {
      UseEvents.events[key] = [];
    }

    UseEvents.events[key].push(callback);

    if (UseEvents.dispatchs[key]) {
      callback(UseEvents.dispatchs[key]);
    }
  }, []);
};

export { ClearEvent, DispatchEvent, OnEvent };
