import { reactLocalStorage } from 'reactjs-localstorage';

//@dynamic
export class Core extends Object
{
    static instance   : any;
    static isCreating : Boolean = false;

    constructor()
    {
        super();

        if(!Core.isCreating)
        {
            throw new Error("You can't call new in Singleton instances!");
        }
    }

    static createInstance()
    {
        throw new Error("esse metodo precisa ser subscrito");
    }

    static get()
    {
        if(Core.instance == null)
        {
            Core.isCreating = true;
            Core.instance   = new Core();
            Core.isCreating = false;
        }

        return Core.instance;
    }

    set languages(values:any)
    {
        const languages : any = {};
        const excludes   = ['referencePath', 'pt'];

        for(const item of values)
        {
            for(const key in item)
            {
                if(!excludes.includes(key))
                {
                    if(!languages[key])
                    {
                        languages[key] = {};        
                    }
    
                    languages[key][item.pt] = item[key];    
                }
            }
        }

        reactLocalStorage.setObject('languages', languages);
    }

    get languages()
    {
        return reactLocalStorage.getObject('languages');
    }

    set selectedLanguage(value:string)
    {
        reactLocalStorage.set('selectedLanguage', value);
    }

    get selectedLanguage()
    {
        return reactLocalStorage.get('selectedLanguage', 'pt');
    }    

    translate(value:string)
    {
        if(Core.get().languages && Core.get().selectedLanguage && 
           Core.get().languages[Core.get().selectedLanguage] && 
           Core.get().languages[Core.get().selectedLanguage][value])
        {
             return Core.get().languages[Core.get().selectedLanguage][value];
        }
   
        return value;
    }
}
