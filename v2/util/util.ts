import axios from "axios";
import { Core } from "./core";
import { call } from "./call.api";
import { Browser } from '@capacitor/browser';
import Types from "../type";
import { TDate } from "../model/TDate";

const translate = (value:string) => 
{
     return Core.get().translate(value);
};

const openUrl = async (url:string) =>
  {
      await Browser.open({ url: url });
  }

const randomString = (length: number) => {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

const sortArrayByProp = (array: any, prop: string) => {
  function SortArray(x: any, y: any) {
    if (x[prop] < y[prop]) {
      return -1;
    }
    if (x[prop] > y[prop]) {
      return 1;
    }
    return 0;
  }

  const s = array.sort(SortArray);

  return s;
};

const randomNumber = (length: number) => {
  var text = "";
  var possible = "0123456789";
  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

const innerHTML = (data: any) => {
  return { __html: data };
};

const stripHtmlTags = (value: string) => {
  if (!value) {
    return "";
  } else {
    const value2 = value.toString();
    return value2.replace(/<[^>]*>/g, "");
  }
};

const removeAccents = (value: string) => {
  const accents: any =
    "ÀÁÂÃÄÅĄĀāàáâãäåąßÒÓÔÕÕÖØŐòóôőõöøĎďDŽdžÈÉÊËĘèéêëęðÇçČčĆćÐÌÍÎÏĪìíîïīÙÚÛÜŰùűúûüĽĹŁľĺłÑŇŃňñńŔŕŠŚŞšśşŤťŸÝÿýŽŻŹžżźđĢĞģğ";
  const accents_out =
    "AAAAAAAAaaaaaaaasOOOOOOOOoooooooDdDZdzEEEEEeeeeeeCcCcCcDIIIIIiiiiiUUUUUuuuuuLLLlllNNNnnnRrSSSsssTtYYyyZZZzzzdGGgg";
  const accents_map = new Map();
  //const value2 		= value.toLowerCase(); // NÃO PODE FAZER ISSO POR CONTA DA BUSCA FULL

  for (const accent in accents) {
    accents_map.set(
      accents.charCodeAt(accent),
      accents_out.charCodeAt(parseInt(accent))
    );
  }

  const nstr = new Array(value.length);
  let x, i;
  for (i = 0; i < nstr.length; i++) {
    nstr[i] = accents_map.get((x = value.charCodeAt(i))) || x;
  }

  return String.fromCharCode.apply(null, nstr);
};

const insertUrlQuery = (key: any, value: any) => {
  if (window.history.pushState) {
    let searchParams = new URLSearchParams(window.location.search);
    searchParams.set(key, value);
    let newurl =
      window.location.protocol +
      "//" +
      window.location.host +
      window.location.pathname +
      "?" +
      searchParams.toString();
    window.history.pushState({ path: newurl }, "", newurl);
  }
};

const findDuplicates = (arr: any) => {
  var newArray = arr?.reduce((unique: any, o: any) => {
    if (
      !unique.some((obj: any) => obj.label === o.label && obj.value === o.value)
    ) {
      unique.push(o);
    }
    return unique;
  }, []);

  return newArray;
};

const objectParser = (object: any) => {
  let objtString: any = [];
  const obj = Object.entries(object).forEach(([key, value]: any) => {
    if (value.quantity > 0) {
      objtString.push(`${key}`);
    }

    // ${value.quantity}
  });

  return objtString;
};

const formatCPF = (cpf: any) => {
  const newcpf = cpf.replace(/\./g, "").replace(/\-/g, "");
  let s = newcpf + "";
  while (s.length < 11) s = "0" + s;
  return s;
};

const objectParserArray = (object: any) => {
  let objtString: any = [];
  const obj = Object.entries(object).forEach(([key, value]: any) => {
    if (value.quantity > 0) {
      objtString.push([key, value.quantity]);
    }

    // ${value.quantity}
  });

  return objtString;
};

const pixToQrCode = async (data: any) => {
  const pixQrCode = await axios.get(
    `https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${data}`
  );

  return pixQrCode;
};

export const buscaCep3 = async (cep: any) => {
  type AddressProps = {
    bairro: string;
    cep: string;
    complemento: string;
    localidade: string;
    logradouro: string;
    uf: string;
  };

  try {
    const response = await fetch(`https://viacep.com.br/ws/${cep}/json/`);
    const address  = await response.json();
    
    if(!address.erro)
    {
      return {
        zipcode  : address.cep,
        city     : address.localidade,
        district : address.bairro,
        state    : address.uf,
        street   : address.logradouro,
        country: { id: "br", label: "Brasil", value: "br" }
      };  
    }

  } catch (error) {
    console.error(error);
  }
};

const buscaCep2 = async (cep: any) => {
  type AddressProps = {
    bairro: string;
    cep: string;
    complemento: string;
    localidade: string;
    logradouro: string;
    uf: string;
    erro:any;
  };

    try {
      const address: AddressProps = (
        await axios.get(`https://viacep.com.br/ws/${cep}/json/`)
      ).data;

      if(!address.erro)
      {
        return {
          zipcode  : address.cep,
          city     : address.localidade,
          district : address.bairro,
          state    : address.uf,
          street   : address.logradouro,
          country: { id: "br", label: "Brasil", value: "br" }
        };  
      }
      
    } catch (error) {
      console.error(error);
    }
};

const buscaCep = async (cep: any, setAddress: any, setValue: any) => {
  type AddressProps = {
    bairro: string;
    cep: string;
    complemento: string;
    localidade: string;
    logradouro: string;
    uf: string;
  };

  try {
    const address: AddressProps = (
      await axios.get(`https://viacep.com.br/ws/${cep}/json/`)
    ).data;
    setAddress(address);
    setValue("city", address.localidade);
    setValue("district", address.bairro);
    setValue("state", address.uf);
    setValue("street", address.logradouro);
  } catch (error) {
    console.error(error);
  }
};

const parseQueryHandler = (
  router: any,
  categories: any,
  colors: any,
  sizes: any,
  stores?: any,
) => {
  const filters: any = [];
  const colorSizeFilters: any = [];
  const minMaxPriceFilters: any = [];

  if (!router.query) {
    return filters;
  }

  const colorValue = colors?.filter(
    (color: any) => color.value == router.query.cor
  );

  const sizeValue = sizes?.filter(
    (size: any) => size.value == router.query.tamanho
  );

  if (stores && router.query.marca) {
    const store = stores.filter(
      (store: any) => store.name == router.query.marca
    );

    colorSizeFilters.push({
      field: "store",
      operator: "==",
      value:  {
          referencePath: store[0].referencePath,
        }
    });
  }

  if (router.query.categoria) {
    const pageCategory = categories.filter(
      (category: any) => category.name == router.query.categoria
    );

    colorSizeFilters.push({
      field: "indexes.categoriesxcolorxsize",
      operator: "combine",
      value: [
        {
          referencePath: pageCategory[0].referencePath,
        },
      ],
    });
  }

  if (router.query.cor) {
    colorSizeFilters.push({
      field: "indexes.categoriesxcolorxsize",
      operator: "combine",
      value: [...colorValue],
    });
  }

  if (router.query.tamanho) {
    colorSizeFilters.push({
      field: "indexes.categoriesxcolorxsize",
      operator: "combine",
      value: [...sizeValue],
    });
  }

  if (router.query.precoMin) {
    minMaxPriceFilters.push({
      field: "indexes.price",
      operator: ">=",
      value: Number(router.query.precoMin),
    });
  }

  if (router.query.precoMax) {
    minMaxPriceFilters.push({
      field: "indexes.price",
      operator: "<",
      value: Number(router.query.precoMax),
    });
  }

  if (router.query) {
    filters.push(...colorSizeFilters, ...minMaxPriceFilters);
  }

  return filters;
};

const orderHandler = (router: any) => {
  let order = {};

  if (router.query.ordenacao === "precoAsc") {
    return (order = { orderBy: "indexes.price", asc: true });
  } else if (router.query.ordenacao === "precoDec") {
    return (order = { orderBy: "indexes.price", asc: false });
  } else if (router.query.ordenacao === "dataAsc") {
    return (order = { orderBy: "postdate", asc: true });
  } else if (router.query.ordenacao === "dataDec") {
    return (order = { orderBy: "postdate", asc: false });
  } else if (router.query.ordenacao == "range") {
    return (order = { orderBy: "indexes.price" });
  } else {
    order = {};
  }

  return order;
};

const textParser = (content: string) => {
  if (content == null || undefined) return;

  const regex = /style="(.*?)"/g;
  const regex2 = /style='(.*?)'/g;
  const subst = ``;

  let result = content.replace(regex, subst);
  result = result.replace(regex2, subst);
  result = result.replace(/<\/?[^>]+(>|$)/g, "");
  result = result.replace(/(<([^>]+)>)/gi, "");
  result = result.replace(/\&nbsp;/g, "");

  return result;
};

const getTotalItems = (items: any) => {
  return items?.reduce((acc: any, item: any) => {
    return (acc = acc + item.quantity);
  }, 0);
};

const getTotalItemsValue = (items: any) => {
  return items?.reduce((acc: any, item: any) => {
    return (acc = acc + item.total);
  }, 0);
};

const getProductsByStore = (items: any) => {
  const stores: any = {};

  items.map((item: any, index: any) => {
    if (!stores[item?.product?.store?.name]) {
      stores[item?.product?.store?.name] = {};
      stores[item?.product?.store?.name].items = [];
    }
    stores[item?.product?.store?.name].store = item?.product?.store;
    stores[item?.product?.store?.name].items.push(item);
  });
  return stores;
};

const loadRSS = (setting:any) => 
{
  const data = {
    url : setting.url,
  };

  return call(Types.RSS_UTIL_SERVER, data)
}
  
function getHTML(data: any): any {
  let teste = { __html: data };
  return teste
}

const replaceDomain = (url:string, domainFrom:any, domainTo:any) => 
{   
    if (domainFrom && domainTo)
    {
        url = url.replace(domainFrom, domainTo);
    }

    return url;
};

const formatDate = (value:any, mask="dd/MM/yyyy") => 
  {
    const date = new TDate({ value : value });
    
    return date.format(mask);
  }

const checkDate = (date:any) =>
{
  let newDate = date
  if(date?.seconds)
  {
    newDate = new Date(date.seconds * 1000)
    return newDate.toISOString() 
  }

  return newDate
}

// const buildObject = (data:any) =>
// {
//   // for(let i of mainObject) Melhorar esse codigo
//   const mainObject:any = {}
//   for(let i = 0; i < data?.length; i++)
//   {
//     mainObject[checkDate(data[i]?.date)] = []
//   }

//   // console.log("mainObject init", mainObject)

//   const keys = Object.keys(mainObject)

//   for(let i = 0; i < keys.length; i++)
//   {
//     for(let j = 0; j < data.length; j++)
//     {
//       if(keys[i] === checkDate(data[j]?.date))
//       {
//         mainObject[keys[i]]?.push(data[j])
//       }
//     }
//   }
//   // console.log('lixo', mainObject)
//   return mainObject
// }

const buildObject = (data: any): Record<string, any[]> => {
  const mainObject: Record<string, any[]> = {};

  // Use filter to group data by date
  const groupedData = data.filter(( item: any ) => item.date).reduce((acc: any, item: any) => {
    const dateKey = checkDate(item.date);
    acc[dateKey] ??= [];
    acc[dateKey].push(item);
    return acc;
  }, mainObject);

  return groupedData;
};

export {
    getProductsByStore,
    getTotalItems,
    randomString,
    orderHandler,
    parseQueryHandler,
    sortArrayByProp,
    randomNumber,
    stripHtmlTags,
    removeAccents,
    insertUrlQuery,
    findDuplicates,
    formatCPF,
    objectParser,
    objectParserArray,
    pixToQrCode,
    buscaCep2,
    buscaCep,
    innerHTML,
    textParser,
    getTotalItemsValue,
    translate,
    openUrl,
    loadRSS,
    getHTML,
    replaceDomain,
    formatDate,
    buildObject
};
