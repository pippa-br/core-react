const customSelectStyles: any = {
    control: (base: any, state: any) => ({
        ...base,
        width: "100%",
        height: "40px",
        borderRadius: "4px",
        borderColor: state.isFocused
        ? "var(--border-color)"
        : "var(--border-color)",
        boxShadow: state.isFocused ? "none" : "",
        "&:hover": {
        borderColor: "var(--border-color)",
        },
        color: "#000",
        backgroundColor: state.hasValue ? "var(--background-button)" : null,
    }),
    singleValue: (styles: any) => ({ ...styles, color: "#fff" }),
    indicatorContainer: (styles: any) => ({ ...styles, padding: "0 8px" }),
    indicatorSeparator: () => "",
    valueContainer: (styles: any, state: any) => ({
        ...styles,
        padding: "0 0 0 5px",
        textAlign: "center",
    }),
    option: (styles: any, state: any) => ({
        ...styles,
        background: "transparent",
        color: "#000",
        transition: "all 0.35s ease",

        "&:hover": {
        background: "var(--background-button)",
        color: "white",
        cursor: "pointer",
        },
    }),
};

export {
  customSelectStyles
}