import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";

const collectionProduct = async (setting:ISetting) => 
{
	const result = await call(Types.COLLECTION_PRODUCT_SERVER, setting);
	
	return result;
}

const getProduct = async (setting:ISetting) => 
{
	const result = await call(Types.GET_PRODUCT_SERVER, setting);
	
	return result;
}

const collection2Product = async (setting:ISetting) => 
{
	const result = await call(Types.COLLECTION_PRODUCT2_SERVER, setting);
	
	return result;
}

const getProduct2 = async (setting:ISetting) => 
{
	const result = await call(Types.GET_PRODUCT2_SERVER, setting);
	
	return result;
}

export { collectionProduct, getProduct, collection2Product, getProduct2 }