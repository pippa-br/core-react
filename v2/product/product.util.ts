import { Matrix } from "../model/matrix"
import { currencyMask } from "../util/mask"

const firstProductImage = (product:any) =>
{
	if(product && product.images)
	{
		for(const key in product.images.data)
		{
			if(product.images.data[key])
			{				
				return product.images.data[key].images[0]
			}
		}
	}
}

const firstProductImageByColor = (product:any, color?: any) =>
{
	if(product && product.images)
	{
		for(const key in product.images.data)
		{
			if(product.images.data[key])
			{ 
				if(color && product?.images?.data?.[color]?.images[0]) {
					return product?.images?.data?.[color]?.images[0]
				}
				return product.images.data[key].images[0]
			}
		}
	}
}

const firstVariantProductImage = (product:any) =>
{
	if(product)
	{
		for(const key in product.images)
		{
			if(product.images[key].images)
			{
				return product.images[key].images[0]
			}			
		}
	}
}

const firstLevel1Variant = (product:any) =>
{
	if(product)
	{
		for(const key in product.variant[0].items)
		{
			return product.variant[0].items[key];
		}
	}
}

const hasStockVariantColor = (product:any, variant:any) =>
{
	if(product)
	{
		product.stockTable.variant = product.variant;
		const stockTable 		   = new Matrix(product.stockTable);
		const data       		   = stockTable.getValueByVariant(variant);

		for(const key in data)
		{
			if(data && data[key].quantity > 0)
			{
				return true;
			}
		}
	}

	return false;
}

const hasStockVariantSize = (product:any, variant1:any, variant2:any) =>
{
	if(product && product.stockTable && variant1 && variant2)
	{
		product.stockTable.variant = product.variant;
		const stockTable 		   = new Matrix(product.stockTable);
		const data     		       = stockTable.getValue([variant1, variant2]);

		if(data && data.quantity > 0)
		{
			return true;
		}
	}

	return false;
}

const quantityStockVariantSize = (product:any, variant1:any, variant2:any) =>
{
	if(product && variant1 && variant2)
	{
		product.stockTable.variant = product.variant;
		const stockTable 		   = new Matrix(product.stockTable);
		const data     		       = stockTable.getValue([variant1, variant2]);

		if(data && data.quantity > 0)
		{
			return data.quantity;
		}
	}

	return 0;
}

// PEGA O PROMOTION OU O PRICE, MENOR VALOR
const productRealPriceValue = (product:any) =>
{
	if(product)
	{
		if(product.promotionalPrice !== undefined && product.promotionalPrice > 0)
		{
			return product.promotionalPrice;
		}		
		else if(product.price !== undefined && product.price > 0)
		{
			return product.price;
		}		
		else if(product.priceTable && product.priceTable.data)
		{
			if(product.priceTable.data._default.promotionalPrice)
			{
				return product.priceTable.data._default.promotionalPrice;
			}
			else
			{
				return product.priceTable.data._default.price;
			}			
		}
		else
		{
			//console.error('no price', product.code, product.priceTable);
		}
	}
}

const productPromotionalPercentage = (product:any) =>
{
	if(product)
	{
		if(product.promotionalPrice !== undefined && product.price !== undefined)
		{
			return Math.round(product.promotionalPrice * 100 / product.price) + '%';
		}		
		else if(product.priceTable && product.priceTable.data)
		{
			if(product.priceTable.data._default.promotionalPrice && product.priceTable.data._default.price)
			{
				return Math.round(product.priceTable.data._default.promotionalPrice * 100 / product.priceTable.data._default.price) + '%';
			}
		}
		
		//console.error('no productPromotionalPercentage', product.code, product.priceTable);
		
		return '0%';
	}
}

const productHasPromotional = (product:any) =>
{
	if(product)
	{
		if(product.promotionalPrice > 0)
		{
			return true;
		}
		else if(product.priceTable && product.priceTable.data)
		{
			if(product.priceTable.data._default.promotionalPrice)
			{
				return true;
			}
		}
		
		return false;
	}
}

const productPromotionalPrice = (product:any) =>
{
	if(product)
	{
		if(product.promotionalPrice !== undefined)
		{
			return currencyMask(product.promotionalPrice);
		}
		else if(product.priceTable && product.priceTable.data)
		{
			if(product.priceTable.data._default.promotionalPrice)
			{
				return currencyMask(product.priceTable.data._default.promotionalPrice);
			}
		}
		
		//console.error('no promotionalPrice', product.code, product.priceTable);
		return currencyMask(0);
	}
}

const productPrice = (product:any) =>
{
	if(product)
	{
		if(product.price !== undefined)
		{
			return currencyMask(product.price);
		}
		else if(product.priceTable && product.priceTable.data)
		{
			if(product.priceTable.data._default.price)
			{
				return currencyMask(product.priceTable.data._default.price);
			}
		}

		///console.error('no price', product.code, product.priceTable);
		return currencyMask(0);
	}
}

const productInstallments = (product: number, installments: number, minInstallment: number) => 
{
	const price : number = productRealPriceValue(product);

	if(price)
	{
		const installmentPerPrice: number = price / minInstallment;
		let finalInstallments: number 	  = Math.floor(installmentPerPrice);
		let pricePerInstallment: number   = price / finalInstallments;
	  
		if(finalInstallments >= installments) 
		{
		  	finalInstallments = installments;
		  	pricePerInstallment = price / finalInstallments;
		}
	  
		if(finalInstallments <= 1) 
		{
		  	return null;
		}

		return `${Number(finalInstallments)}x de ${currencyMask(
			Number(pricePerInstallment.toFixed(2))
		)} sem juros`;
	}
	else
	{
		return null;
	}	
};

const productImagesVariant = (product: any, variantValue: any) =>
{   
    const images = product?.images?.data[variantValue]?.images;

    return images;
}

const productVariantByValue = (product: any, variantValue: any) =>
{
	for(const variant of product.variant)
	{
		for(const item of variant.items)
		{
			if(item.value == variantValue)
			{
				return item;
			}
		}
	}
}

const productVariantByLabel = (product: any, variantValue: any) =>
{
	for(const variant of product.variant)
	{
		for(const item of variant.items)
		{
			if(item.label == variantValue)
			{
				return item;
			}
		}
	}
}

const productColorVariant = (router: any, colors: any) => 
{
	let variant : any = null;

	// COR
	if(router.query.color) 
	{
		variant = colors?.filter
		(
			(color: any) => color.label == router.query.color
		);
	}	

	return variant;
};

const productParseQuery =  (router: any, categories:any, colors: any, sizes: any) => 
{
	const filters: any = [];
  
	if(!router.query) 
	{
	  	return filters;
	}
    
	// CATEGORIAS
	if(categories) 
	{
		for(const item of categories)
		{
			if(item)
			{
				filters.push({
					field: "indexes.categoriesxcolorxsize",
					operator: "combine",
					value: [
					{
						referencePath: item.referencePath,
					},
				]});	
			}
		}	  
	}
  
	// COR
	if(router.query.color) 
	{
		const colorValue = colors?.filter
		(
			(color: any) => color.label == router.query.color
		);
	
		filters.push({
			field: "indexes.categoriesxcolorxsize",
			operator: "combine",
			value: [...colorValue],
		});
	}
  
	// TAMANHO
	if(router.query.size) 
	{
		const sizeValue = sizes?.filter(
			(size: any) => size.value == router.query.size
	  	);

		  filters.push({
			field: "indexes.categoriesxcolorxsize",
			operator: "combine",
			value: [...sizeValue],
	  	});
	}

	// PRECO MIN
	if(router.query.precoMin) 
	{
		filters.push({
			field: "indexes.price",
			operator: ">=",
			value: Number(router.query.precoMin)
		});
	}

	// PRECO MAX
	if(router.query.precoMax) 
	{
		filters.push({
			field: "indexes.price",
			operator: "<",
			value: Number(router.query.precoMax)
		});
	}
    
	return filters;
};

export { 
	firstProductImage, 
	firstProductImageByColor,
	firstVariantProductImage, 
	hasStockVariantSize,
	productPrice, 
	hasStockVariantColor,
	quantityStockVariantSize,
	productInstallments, 
	productVariantByValue,
	firstLevel1Variant,
	productImagesVariant, 
	productParseQuery, 
	productPromotionalPrice, 
	productVariantByLabel, 
	productColorVariant, 
	productHasPromotional,
	productPromotionalPercentage
}


