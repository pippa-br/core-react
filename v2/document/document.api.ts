import Types from "../type";
import { ISetting } from "../util/setting";
import { call } from "../util/call.api";
import { useEffect } from "react";

const useCollectionDocument = async (setting:ISetting, onSuccess:any) => 
{
	useEffect(() => 
	{
		call(Types.COLLECTION_DOCUMENT_SERVER, setting).then(result => 
		{
			onSuccess(result)
		});
	}, []);	
}

const useGetDocument = async (setting:ISetting, onSuccess:any) => 
{
	useEffect(() => 
	{
		call(Types.GET_DOCUMENT_SERVER, setting).then(result => 
		{
			onSuccess(result)
		});
	}, []);	
}

const collectionOwnerDocument = async (setting: ISetting) => 
{
    const result = await call(Types.COLLECTION_OWNER_DOCUMENT_SERVER, setting);

    return result;
}

const collectionDocument = async (setting:ISetting) => 
{
	const result = await call(Types.COLLECTION_DOCUMENT_SERVER, setting);
	
	return result;
}

const countDocument = async (setting: ISetting) => {
	const result = await call(Types.COUNT_DOCUMENT_SERVER, setting);
	
	return result;
}

// const useGetDocument = async (setting:ISetting) => 
// {
// 	const result = await call(Types.GET_DOCUMENT_SERVER, setting);
	
// 	return result;
// }

const addDocument = async (setting:ISetting) => 
{
	const result = await call(Types.ADD_DOCUMENT_SERVER, setting);
	
	return result;
}

const setDocument = async (setting:ISetting) => 
{
	const result = await call(Types.SET_DOCUMENT_SERVER, setting);
	
	return result;
}

const incrementDocument = async (setting:ISetting) => 
{
	const result = await call(Types.INCREMENT_DOCUMENT_SERVER, setting);
	
	return result;
}


const cancelDocument = async (setting:ISetting) => 
{
	const result = await call(Types.DELETE_DOCUMENT_SERVER, setting);
	
	return result;
}

const setViews = async (setting:ISetting) => 
{
	const result = await call(Types.SET_VIEWS_SERVER, setting);
	
	return result;
}

const getDocument = async (setting: ISetting) => {
	const result = await call(Types.GET_DOCUMENT_SERVER, setting);

	return result;
}

export { 
	useCollectionDocument, 
	useGetDocument,
	collectionDocument,
	countDocument,
	// useGetDocument, 
	addDocument, 
	setViews, 
	setDocument, 
	cancelDocument, 
	incrementDocument,
	getDocument,
	collectionOwnerDocument
}