import Types from "../type";
import { ISetting } from "../util/setting";
import { call, callForm, callFormData } from "../util/call.api";

const uploadTemp = async (setting:ISetting) => 
{
	const result = await callForm(Types.UPLOAD_TEMP_SERVER, setting);
	
	return result;	
}

const base64Upload = async(setting:ISetting) => {
	const result = await call(Types.UPLOAD_BASE_64, setting);
	
	return result;	
}

const uploadStorage = async (setting:any) => 
{
	const form = new FormData();
	const keys = Object.keys(setting);

	for(let i=0; i<keys.length; i++){
		form.append(keys[i], setting[keys[i]]);
	}

	const result = await callFormData(Types.UPLOAD_STORAGE_SERVER, form);
	
	return result;	
}

export { uploadTemp, base64Upload, uploadStorage }